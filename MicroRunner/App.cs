﻿using Services;
using MicroRunner;
using MicroRunner.Services.DTO;

public class App
{
    private readonly HttpClient _httpClient = new HttpClient();
    private readonly SessionService _sessionService;
    private readonly MazeService _mazeService;

    private readonly Dictionary<HostConfiguration, Uri> _configMap = new()
    {
        {HostConfiguration.Local, new("http://localhost:8000/")},
        {HostConfiguration.Ngrok, new("https://a63a-2001-1c03-4f11-1900-3867-c2e3-3ccb-5eba.ngrok-free.app/")},
        {HostConfiguration.Martijn, new("http://83.86.116.124:8000/")}
    };

    public App(HostConfiguration config)
    {
        _httpClient.BaseAddress = _configMap[config];

        UserIn userIn = new UserIn("plsPint", "pepe420420");
        _sessionService = new SessionService(_httpClient, userIn);
        string token = _sessionService.Login().Result;

        _mazeService = new(_httpClient, _sessionService);

        Console.WriteLine("Connected to MazeService");
    }

    public async Task Run()
    {
        Mouse mouse = new(_mazeService);
        await mouse.Initialize(10);
        await mouse.RunMaze();
    }
}
