﻿using System.Globalization;

Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

HostConfiguration config = HostConfiguration.Local;
Console.WriteLine($"Using config: {config}");

App app = new(config);
await app.Run();

Console.WriteLine("Finished run");
Console.ReadLine();