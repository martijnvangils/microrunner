﻿namespace MicroRunner.Maze
{
    public struct Wall
    {
        public Wall(Cell a, Cell b)
        {
            A = a;
            B = b;
        }

        public Cell A { get; set; }
        public Cell B { get; set; }

        public override string ToString()
        {
            return $"Wall between :\n" +
                   $"{A} \n" +
                   $"{B}";
        }
    }
}