﻿using MicroRunner.Services.DTO;
using System.Xml.Linq;

namespace MicroRunner.Maze
{
    public class Maze
    {
        public delegate void WallAddedEventHandler(object sender, WallAddedEventArgs e);
        public event EventHandler<WallAddedEventArgs> WallAddedEvent;

        public delegate void MazeFloodedEventHandler(object sender, MazeFloodedEventArgs e);
        public event EventHandler<MazeFloodedEventArgs> MazeFloodedEvent;

        public delegate void MazeInitializedEventHandler(object sender, MazeInitializedEventArgs e);
        public event EventHandler<MazeInitializedEventArgs> MazeInitializedEvent;

        // Cell index [4, 2] has middle coordinate [4.5, 2.5]
        private List<Cell> _cells = new();
        private List<Wall> _walls = new();
        private double _size;

        private Cell _finishCell => _cells.Where(x => x.IsFinish).First();

        public XY FinishPosition
        {
            get; private set;
        }

        public Maze(MazeInformation mazeInfo, int mazeSize)
        {
            FinishPosition = new(mazeInfo.FinishCellX + 0.5, mazeInfo.FinishCellY + 0.5);
            _size = mazeSize;
        }

        public void Initialize()
        {
            LoadMaze((int)_size);
            FloodMaze();
        }

        public Direction GetNextMove(XY mousePosition)
        {
            Cell cell = GetMouseCell(mousePosition);
            Dictionary<Direction, int?> possibleMoves = new();

            foreach (Direction direction in Enum.GetValues(typeof(Direction)))
            {
                Cell? adjacent = GetAdjacentCell(cell, direction);
                if (adjacent is null) continue;

                if (!IsWallKnownBetweenCells(cell, adjacent))
                {
                    possibleMoves.Add(direction, adjacent.DistanceToFinish);
                }
            }

            return possibleMoves.OrderBy(x => x.Value).First().Key;
        }

        private Cell GetMouseCell(XY mousePosition)
        {
            return _cells.Where(x => x.MiddlePoint.Equals(mousePosition)).First();
        }

        private void LoadMaze(int mazeSize)
        {
            for (int y = 0; y < mazeSize; y++)
            {
                LoadRow(mazeSize, y);
            }

#if DEBUG
            int finishCells = _cells.Where(x => x.IsFinish).ToList().Count;
            if (finishCells != 1)
            {
                throw new Exception($"Amount of finish cells is incorrect. Value is: {finishCells}");
            }
#endif
            MazeInitializedEvent?.Raise<MazeInitializedEventArgs>(this, new(_cells));
        }

        private void LoadRow(int mazeSize, int y)
        {
            for (int x = 0; x < mazeSize; x++)
            {
                XY currentCell = new XY(x, y);
                XY finishCell = new XY(Math.Floor(FinishPosition.X), Math.Floor(FinishPosition.Y));
                Cell cell = new(x, y, finishCell.Equals(currentCell));

#if DEBUG
                //Console.WriteLine($"Adding {cell}");
#endif
                _cells.Add(cell);
            }
        }

        private bool IsWallKnownBetweenCells(Cell a, Cell b)
        {
            List<Wall> query = _walls
                                .Where(x => x.A == a && x.B == b || x.B == a && x.A == b)
                                .ToList();
#if DEBUG
            if (query.Count > 1) throw new Exception("Invalid state: Multiple walls have been found between 2 cells");
            //Console.WriteLine(
            //    $"Wall found between cells: \n" +
            //    $"{a} \n" +
            //    $"{b}");
#endif
            return query.Count > 0;
        }

        private void FloodMaze()
        {
            _cells.ForEach(x => x.UpdateDistance(null));
            _finishCell.UpdateDistance(0);

            List<Cell> inputCells = new() { _finishCell };

            while (inputCells.Count > 0)
            {
                inputCells = FloodAdjacentCells(inputCells);
            }

            MazeFloodedEvent.Raise<MazeFloodedEventArgs>(this, new(_cells));
        }

        private List<Cell> FloodAdjacentCells(List<Cell> inputCells)
        {
            List<Cell> validNeighbours = new();

            foreach (Cell cell in inputCells)
            {
                foreach (Direction direction in Enum.GetValues(typeof(Direction)))
                {
                    Cell? adjacent = GetAdjacentCell(cell, direction);

                    if (adjacent is null) continue;
                    if (adjacent.DistanceToFinish is not null) continue;
                    if (IsWallKnownBetweenCells(cell, adjacent)) continue;

                    int? newDistance = cell.DistanceToFinish + 1 ?? null;

#if DEBUG
                    if (newDistance is null)
                    {
                        throw new Exception(
                            $"FloodAdjacent Cells \n " +
                            $"{adjacent} \n " +
                            $"Distance from previous cell is null");
                    }
#endif
                    _cells.Where(x => x.Equals(adjacent))
                          .First()
                          .UpdateDistance(newDistance);

#if DEBUG
                    //Console.WriteLine($"[{adjacent}] Updated distance value to {newDistance}");
#endif
                    validNeighbours.Add(adjacent);
                }
            }

            return validNeighbours;
        }

        private Cell? GetAdjacentCell(Cell input, Direction direction)
        {
            try
            {
                return direction switch
                {
                    Direction.North => _cells.Where(x => input.Index.X == x.Index.X && input.Index.Y + 1 == x.Index.Y).First(),
                    Direction.South => _cells.Where(x => input.Index.X == x.Index.X && input.Index.Y - 1 == x.Index.Y).First(),
                    Direction.East => _cells.Where(x => input.Index.X + 1 == x.Index.X && input.Index.Y == x.Index.Y).First(),
                    Direction.West => _cells.Where(x => input.Index.X - 1 == x.Index.X && input.Index.Y == x.Index.Y).First(),
                    _ => null,
                };
            }
            catch
            {
                return null;
            }
        }

        public void AddWall(XY position, Direction direction)
        {
            Cell cell = GetMouseCell(position);
            Cell? otherCell = GetAdjacentCell(cell, direction);

#if DEBUG
            if (otherCell is null) throw new Exception("Couldn't add wall to maze, other cell is null");
#endif
            Wall newWall = new(cell, otherCell);
            _walls.Add(newWall);
#if DEBUG
            Console.WriteLine($"New wall added: {newWall}");
#endif
            FloodMaze();
            WallAddedEvent?.Raise<WallAddedEventArgs>(this, new(newWall));
        }
    }
}