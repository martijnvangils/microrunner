﻿using MicroRunner.Maze;

public class WallAddedEventArgs
{
    public WallAddedEventArgs(Wall wall) 
    { 
        AddedWall = wall; 
    }

    public Wall AddedWall { get; private set; }
}
