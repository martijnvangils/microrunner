﻿using MicroRunner;
using MicroRunner.Maze;
using MicroRunner.Services.DTO;
using Services;

public class Mouse
{
    public delegate void MovedEventHandler(object sender, MovedEventArgs e);
    public event EventHandler<MovedEventArgs> MovedEvent;

    public event EventHandler UpdateUI;

    private readonly MazeService _mazeService;

    private Maze _maze;
    public Maze Maze => _maze;

    private XY _position;

    public bool Alive { get; private set; } = true;
    public object Dispatcher { get; private set; }

    public Mouse(MazeService mazeService)
    {
        _mazeService = mazeService;
    }

    public async Task Initialize(int mazeSize)
    {
        var mazeInfo = await _mazeService.InitializeMaze(mazeSize).ConfigureAwait(false);
        Console.WriteLine($"Initialized maze with size: {mazeSize}");
        _maze = new(mazeInfo, mazeSize);
    }

    public async Task<bool> RunMaze()
    {
        MouseInformation mouseInfo = await _mazeService.RegisterMouseToMaze();

        _position = new(mouseInfo.X, mouseInfo.Y);
        MoveOut moveOut = new();
        moveOut.X = mouseInfo.X;
        moveOut.Y = mouseInfo.Y;
        MovedEvent?.Raise<MovedEventArgs>(this, new(moveOut));

        while ((_position.X != _maze.FinishPosition.X || _position.Y != _maze.FinishPosition.Y) && Alive)
        {
            Direction direction = _maze.GetNextMove(_position);
            Thread.Sleep(400);
            bool canMove = ScanDirection(direction);
            Thread.Sleep(400);
            if (canMove)
            {
                MoveInDirection(direction);
            }
            else
            {
                _maze.AddWall(_position, direction);
            }

            UpdateUI?.Raise<EventArgs>(this, new());
        }

        return Alive;
    }

    private void MoveInDirection(Direction confirmedDirection)
    {
        double xMoveTo = _position.X;
        double yMoveTo = _position.Y;
        switch (confirmedDirection)
        {
            case Direction.North:
                yMoveTo++;
                break;
            case Direction.East:
                xMoveTo++;
                break;
            case Direction.South:
                yMoveTo--;
                break;
            case Direction.West:
                xMoveTo--;
                break;
        }

        PositionIn positionIn = new(xMoveTo, yMoveTo);
        MoveOut moveOut = _mazeService.MoveMouse(positionIn).Result;
        XY newPosition = new(moveOut.X, moveOut.Y);

        _position = newPosition;

#if DEBUG
        Console.WriteLine($"Moved to {_position}");
#endif

        Alive = !moveOut.End || (_position.Equals(_maze.FinishPosition));

        MovedEvent?.Raise<MovedEventArgs>(this, new(moveOut));
    }

    private bool ScanDirection(Direction direction)
    {
        ScanIn scanIn = new((int)direction);
        ScanOut scanOut = _mazeService.Scan(scanIn).Result;
        Console.WriteLine($"Scanresult direction {direction} is {scanOut.Detected}");

        return !scanOut.Detected;
    }
}


public static class Extensions
{

    public static T Next<T>(this T src) where T : struct
    {
        if (!typeof(T).IsEnum) throw new ArgumentException(String.Format("Argument {0} is not an Enum", typeof(T).FullName));

        T[] Arr = (T[])Enum.GetValues(src.GetType());
        int j = Array.IndexOf<T>(Arr, src) + 1;
        return (Arr.Length == j) ? Arr[0] : Arr[j];
    }
}