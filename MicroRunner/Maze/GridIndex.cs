﻿using System.Diagnostics.CodeAnalysis;

public struct GridIndex
{
    public GridIndex(int x, int y)
    {
        X = x;
        Y = y;
    }

    public int X { get; }
    public int Y { get; }

    public override string ToString()
    {
        return $"Index:[{X},{Y}]";
    }

    public override bool Equals([NotNullWhen(true)] object? obj) => obj is GridIndex && this.Equals(obj);

    public bool Equals(GridIndex other)
    {
        return this.X == other.X && this.Y == other.Y;
    }
}