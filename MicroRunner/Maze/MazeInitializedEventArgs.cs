﻿namespace MicroRunner.Maze
{
    public class MazeInitializedEventArgs
    {
        public MazeInitializedEventArgs(List<Cell> cells)
        {
            Cells = cells;
        }

        public List<Cell> Cells { get; private set; }
    }
}