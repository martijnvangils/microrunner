﻿using Services;

public class MovedEventArgs
{
    public MovedEventArgs(MoveOut move) 
    {  
        Move = move;
    }

    public MoveOut Move { get; }
}
