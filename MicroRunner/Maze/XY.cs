﻿using System.Diagnostics.CodeAnalysis;
using System.Security.Cryptography.X509Certificates;

public struct XY
{
    public readonly double X;
    public readonly double Y;

    public XY(double x, double y)
    {
        X = x;
        Y = y;
    }

    public override bool Equals([NotNullWhen(true)] object? obj) => obj is XY && this.Equals(obj);

    public bool Equals(XY other)
    {
        // Only need value-based comparison
        return this.X == other.X && this.Y == other.Y;
    }

    public override string ToString()
    {
        return $"Position:[{X},{Y}]";
    }
}
