﻿namespace MicroRunner.Maze
{
    public class MazeFloodedEventArgs
    {
        public List<Cell> Cells;

        public MazeFloodedEventArgs(List<Cell> cells)
        {
            Cells = cells;
        }
    }
}