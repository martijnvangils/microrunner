﻿using System.Diagnostics.CodeAnalysis;

public class Cell
{
    public GridIndex Index { get; private set; }
    public bool IsFinish { get; private set; }
    public XY MiddlePoint { get; private set; }

    public int? DistanceToFinish { get; private set; }

    public Cell(int xIndex, int yIndex, bool isFinish)
    {
        Index = new(xIndex, yIndex);
        IsFinish = isFinish;

        double middleX = xIndex + 0.5;
        double middleY = yIndex + 0.5;
        MiddlePoint = new(middleX, middleY);
    }

    public void UpdateDistance(int? distance)
    {
        DistanceToFinish = distance;
    }

    public override string ToString()
    {
        return $"Cell: {MiddlePoint} | {Index} | {IsFinish}";
    }

    public override bool Equals([NotNullWhen(true)] object? obj) => obj is Cell && this.Equals(obj);

    public bool Equals(Cell other)
    {
        return this.Index.Equals(other.Index);
    }
}
