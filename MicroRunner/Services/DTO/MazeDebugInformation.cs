﻿//----------------------
// <auto-generated>
//     Generated using the NSwag toolchain v13.19.0.0 (NJsonSchema v10.9.0.0 (Newtonsoft.Json v13.0.0.0)) (http://NSwag.org)
// </auto-generated>
//----------------------

#pragma warning disable 108 // Disable "CS0108 '{derivedDto}.ToJson()' hides inherited member '{dtoBase}.ToJson()'. Use the new keyword if hiding was intended."
#pragma warning disable 114 // Disable "CS0114 '{derivedDto}.RaisePropertyChanged(String)' hides inherited member 'dtoBase.RaisePropertyChanged(String)'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword."
#pragma warning disable 472 // Disable "CS0472 The result of the expression is always 'false' since a value of type 'Int32' is never equal to 'null' of type 'Int32?'
#pragma warning disable 612 // Disable "CS0612 '...' is obsolete"
#pragma warning disable 1573 // Disable "CS1573 Parameter '...' has no matching param tag in the XML comment for ...
#pragma warning disable 1591 // Disable "CS1591 Missing XML comment for publicly visible type or member ..."
#pragma warning disable 8073 // Disable "CS8073 The result of the expression is always 'false' since a value of type 'T' is never equal to 'null' of type 'T?'"
#pragma warning disable 3016 // Disable "CS3016 Arrays as attribute arguments is not CLS-compliant"
#pragma warning disable 8603 // Disable "CS8603 Possible null reference return"

namespace Services
{
    using System = System;
    [System.CodeDom.Compiler.GeneratedCode("NJsonSchema", "13.19.0.0 (NJsonSchema v10.9.0.0 (Newtonsoft.Json v13.0.0.0))")]
    public partial class MazeDebugInformation
    {
        [Newtonsoft.Json.JsonProperty("id", Required = Newtonsoft.Json.Required.Always)]
        [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
        public Guid Id { get; set; }

        [Newtonsoft.Json.JsonProperty("start_cell", Required = Newtonsoft.Json.Required.Always)]
        [System.ComponentModel.DataAnnotations.Required]
        [System.ComponentModel.DataAnnotations.MinLength(2)]
        [System.ComponentModel.DataAnnotations.MaxLength(2)]
        public Tuple<int, int> Start_cell { get; set; } = new Tuple<int, int>(0, 0);

        [Newtonsoft.Json.JsonProperty("finish_cell", Required = Newtonsoft.Json.Required.Always)]
        [System.ComponentModel.DataAnnotations.Required]
        [System.ComponentModel.DataAnnotations.MinLength(2)]
        [System.ComponentModel.DataAnnotations.MaxLength(2)]
        public Tuple<int, int> Finish_cell { get; set; } = new Tuple<int, int>(0, 0);

        //[Newtonsoft.Json.JsonProperty("seed", Required = Newtonsoft.Json.Required.Always)]
        //public Seed Seed { get; set; }

        //[Newtonsoft.Json.JsonProperty("map", Required = Newtonsoft.Json.Required.AllowNull)]
        //public string Map { get; set; }
        //private IDictionary<string, object> _additionalProperties;

        //[Newtonsoft.Json.JsonExtensionData]
        //public IDictionary<string, object> AdditionalProperties
        //{
        //    get { return _additionalProperties ?? (_additionalProperties = new Dictionary<string, object>()); }
        //    set { _additionalProperties = value; }
        //}

    }
}

#pragma warning restore 108
#pragma warning restore 114
#pragma warning restore 472
#pragma warning restore 612
#pragma warning restore 1573
#pragma warning restore 1591
#pragma warning restore 8073
#pragma warning restore 3016
#pragma warning restore 8603