﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace MicroRunner.Services.DTO
{
    public class MazeInitialization
    {
        public MazeInitialization(int size, string? seed = null, bool debug = false)
        {
            Size = size;
            Seed = seed;
            Debug = debug;
        }

        [JsonPropertyName("size")]
        public int Size { get; set; } = 5;

        [JsonPropertyName("seed")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string? Seed { get; set; } = null;

        [JsonPropertyName("debug")]
        public bool Debug { get; set; } = false;
    }
}
