﻿namespace Services
{
    using System.Text.Json.Serialization;
    public class ScanOut
    {
        [JsonPropertyName("x")]
        public double X { get; set; }

        [JsonPropertyName("y")]
        public double Y { get; set; }

        [JsonPropertyName("action_count")]
        public int Action_count { get; set; }

        [JsonPropertyName("end")]
        public bool End { get; set; } = false;

        [JsonPropertyName("detected")]
        public bool Detected { get; set; }
    }
}