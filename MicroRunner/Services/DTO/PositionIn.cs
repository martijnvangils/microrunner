﻿using System.Text.Json.Serialization;

namespace MicroRunner.Services.DTO
{
    public class PositionIn
    {
        public PositionIn(double x, double y)
        {
            X = x;
            Y = y;
        }

        [JsonPropertyName("x")]
        public double X { get; set; }

        [JsonPropertyName("y")]
        public double Y { get; set; }
    }
}
