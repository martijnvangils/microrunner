﻿using System.Text.Json.Serialization;

namespace MicroRunner.Services.DTO
{
    public class MazeInformation
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("start_cell")]
        public List<double> Start_cell { get; set; } = new List<double>();

        [JsonPropertyName("finish_cell")]
        public List<double> Finish_cell { get; set; } = new List<double>();

        [JsonIgnore]
        public double FinishCellX => Finish_cell[0];

        [JsonIgnore]
        public double FinishCellY => Finish_cell[1];
    }
}