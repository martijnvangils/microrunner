﻿using System.Text.Json.Serialization;

namespace Services
{
    public partial class ScanIn
    {
        //public ScanIn(ICollection<int> angles, double radius = 1D)
        //{
        //    Angles = angles;
        //    Radius = radius;
        //}

        public ScanIn(int angle, double radius = 1D)
        {
            Angles.Add(angle);
            Radius = radius;
        }

        [JsonPropertyName("angles")]
        public ICollection<int> Angles { get; set; } = new System.Collections.ObjectModel.Collection<int>();

        [JsonPropertyName("radius")]
        public double Radius { get; set; } = 1D;
    }
}