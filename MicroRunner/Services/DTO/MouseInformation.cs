﻿using System.Text.Json.Serialization;

namespace MicroRunner.Services.DTO
{
    public class MouseInformation
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("token")]
        public string Token { get; set; }

        [JsonPropertyName("position")]
        public List<double> Position { get; set; } = new List<double>();

        [JsonIgnore]
        public double X => Position[0];

        [JsonIgnore]
        public double Y => Position[1];
    }
}