﻿namespace Services
{
    using System.Text.Json.Serialization;
    public class UserIn
    {
        public UserIn(string username, string password)
        {
            Username = username;
            Password = password;
        }

        [JsonPropertyName("username")]
        public string Username { get; set; }

        [JsonPropertyName("password")]
        public string Password { get; set; }
    }
}