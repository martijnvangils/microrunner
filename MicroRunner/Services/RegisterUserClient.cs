﻿using Services;
using System.Net.Http.Json;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace MicroRunner
{
    public class RegisterUserClient
    {
        private readonly string _endPoint = "account/register";
        private readonly HttpClient _client;
        private readonly SessionService _sessionClient;

        public RegisterUserClient(HttpClient client, SessionService sessionClient)
        {
            _client = client;
            _sessionClient = sessionClient;
        }

        public async Task<object> PutAsync(UserIn user)
        {
            var json = JsonSerializer.Serialize(user);

            var jsonContent = new StringContent(json, Encoding.UTF8, "application/json");
            jsonContent.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("application/json");

            HttpRequestMessage request = new(HttpMethod.Put, _endPoint);
            request.Content = jsonContent;
            var response = await _client.SendAsync(request);

            return response;
        }
    }
}
