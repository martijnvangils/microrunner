﻿using Services;
using System.Text;
using System.Text.Json;
using System.Net.Http;
using Microsoft.Extensions.Caching.Memory;

namespace MicroRunner
{
    public class SessionService
    {
        private readonly string _tokenEndPoint = "account/token";
        private readonly string _refreshEndPOint = "account/refresh";
        private readonly HttpClient _client;
        private readonly UserIn _user;
        private string _sessionCookie;
        private string _cookie;

        private readonly MemoryCache _cache;

        public SessionService(HttpClient client, UserIn user)
        {
            _client = client;
            _user = user;
            _client.DefaultRequestHeaders.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/json"));
            _cache = new(new MemoryCacheOptions());
        }

        public async Task<string> Login()
        {
            var json = JsonSerializer.Serialize(_user);

            var jsonContent = new StringContent(json, Encoding.UTF8, "application/json");
            jsonContent.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("application/json");

            HttpRequestMessage request = new(HttpMethod.Post, _tokenEndPoint);
            request.Content = jsonContent;
            var response = await _client.SendAsync(request).ConfigureAwait(false);

            if(response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new Exception("Unauthorized login");
            }

            _sessionCookie = (await response.Content.ReadAsStringAsync()).Replace("\"", "");
            _cookie = response.Headers.GetValues("Set-Cookie").First().ToString().Replace("\"", "");
            return _sessionCookie;
        }

        public async Task<string> Refresh()
        {
            if (_cache.TryGetValue("session_cookie", out string? cachedValue) && cachedValue is not null)
            {
                return cachedValue;
            }

            HttpRequestMessage request = new(HttpMethod.Post, _refreshEndPOint);
            request.Headers.Add("Set-cookie", _cookie);
            var response = await _client.SendAsync(request).ConfigureAwait(false);

            _sessionCookie = (await response.Content.ReadAsStringAsync()).Replace("\"", "");
            _cache.Set("session_cookie", _sessionCookie, TimeSpan.FromMinutes(14));

            _cookie = response.Headers.GetValues("Set-Cookie").First().ToString().Replace("\"", "");

            return _sessionCookie;
        }
    }
}