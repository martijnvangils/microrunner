﻿using MicroRunner.Services.DTO;
using Services;
using System.Text;
using System.Text.Json;

namespace MicroRunner
{
    public class MazeService
    {
        private Guid _mazeId;
        private Guid _mouseId;
        private string _mouseToken;
        
        private readonly HttpClient _client;
        private readonly SessionService _sessionClient;

        private readonly string _initializeEndpoint = "maze/initialize";
        private readonly string _registerMouseEndpoint = "maze/{0}/register";
        private readonly string _moveMouseEndpoint = "maze/{0}/move";
        private readonly string _scanMouseEndpoint = "maze/{0}/scan";

        public MazeService(HttpClient client, SessionService sessionClient)
        {
            _client = client;
            this._sessionClient = sessionClient;
        }

        public async Task<Output> PutAsync<Output>(string endpoint, HttpMethod method)
        {
            return await PutAsync<Output, object>(endpoint, method, null).ConfigureAwait(false);
        }

        public async Task<Output> PutAsync<Output, Input>(string endpoint, HttpMethod method, Input? content)
        {
            HttpRequestMessage request = new(method, endpoint);
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", await _sessionClient.Refresh());

            if (content != null)
            {
                string requestJson = JsonSerializer.Serialize<Input>(content);

                StringContent jsonContent = new(requestJson, Encoding.UTF8, "application/json");
                jsonContent.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("application/json");

                request.Content = jsonContent;
            }

            HttpResponseMessage response = await _client.SendAsync(request).ConfigureAwait(false);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Pls");
            }

            string json = await response.Content.ReadAsStringAsync();
            Output? info = JsonSerializer.Deserialize<Output>(json);
            if (info == null)
            {
                throw new Exception("There was no info returned from the ");
            }

            return info;
        }

        public async Task<MazeInformation> InitializeMaze(int mazeSize)
        {
            MazeInitialization mazeInfo = new(mazeSize);
#if DEBUG
            //MazeInformation info = new();
            //info.Start_cell.Add(1);
            //info.Start_cell.Add(1);
            //info.Finish_cell.Add(6);
            //info.Finish_cell.Add(6);
            ////info.Id = new Guid("25f33b86-a82d-4b7d-bb73-cf410a0ad39a");
            //info.Id = new Guid("6bfa963c-6706-4597-8579-7b8862ffc2fb");
            //_mazeId = info.Id;
            MazeInformation info = await PutAsync<MazeInformation, MazeInitialization>(_initializeEndpoint, HttpMethod.Put, mazeInfo).ConfigureAwait(false);
            _mazeId = info.Id;
#else
            MazeInformation info = await PutAsync<MazeInformation, MazeInitialization>(_initializeEndpoint, HttpMethod.Put, mazeInfo).ConfigureAwait(false);
            _mazeId = info.Id;
#endif
            return info;
        }

        public async Task<MouseInformation> RegisterMouseToMaze()
        {
#if DEBUG
            string formattedEndpoint = String.Format(_registerMouseEndpoint, _mazeId);
#else
            string formattedEndpoint = String.Format(_registerMouseEndpoint, _mazeId);
#endif

            MouseInformation info = await PutAsync<MouseInformation>(formattedEndpoint, HttpMethod.Put).ConfigureAwait(false);

            _mouseId = info.Id;
            _mouseToken = info.Token;
            _client.DefaultRequestHeaders.Add("Mousorization", _mouseToken);
            return info;
        }

        public async Task<MoveOut> MoveMouse(PositionIn move)
        {
            string formattedEndpoint = String.Format(_moveMouseEndpoint, _mazeId);
            return await PutAsync<MoveOut, PositionIn>(formattedEndpoint, HttpMethod.Post, move).ConfigureAwait(false);
        }
        
        public async Task<ScanOut> Scan(ScanIn scanIn)
        {
            string formattedEndpoint = String.Format(_scanMouseEndpoint, _mazeId);
            return await PutAsync<ScanOut, ScanIn>(formattedEndpoint, HttpMethod.Post, scanIn).ConfigureAwait(false);
        }
    }
}