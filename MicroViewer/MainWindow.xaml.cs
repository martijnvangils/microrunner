﻿using MicroRunner;
using MicroRunner.Maze;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using static System.Formats.Asn1.AsnWriter;

namespace MicroViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private HttpClient _httpClient = new();
        private SessionService _sessionService;
        private MazeService _mazeService;
        private Mouse _mouse;

        private Rectangle _mousePosition = new Rectangle();
        private List<TextBlock> _currentDistances = new List<TextBlock>();

        private const int MAZE_SIZE = 20;

        private double _gScale => Width / ((double)MAZE_SIZE * 1.1);

        private readonly Dictionary<HostConfiguration, Uri> _configMap = new()
        {
                { HostConfiguration.Local, new("http://localhost:8000/")},
                { HostConfiguration.Ngrok, new("https://a63a-2001-1c03-4f11-1900-3867-c2e3-3ccb-5eba.ngrok-free.app/")},
                { HostConfiguration.Martijn, new("http://83.86.116.124:8000/")}
        };

        public MainWindow()
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            double size = 1 * _gScale;

            _mousePosition.Height = size;
            _mousePosition.Width = size;
            _mousePosition.Stroke = Brushes.Blue;
            _mousePosition.Fill = Brushes.Blue;
        }

        private void _mouse_WallAddedEvent(object? sender, WallAddedEventArgs e)
        {
            GridIndex indexA = e.AddedWall.A.Index;
            GridIndex indexB = e.AddedWall.B.Index;

            double x1, x2, y1, y2;
            double size = 1 * _gScale;

            double left;
            double bottom;

            if (indexA.X == indexB.X)
            {
                //Horizontal
                double highest = Math.Max(indexA.Y, indexB.Y);

                x1 = 0;             
                x2 = size;
                y1 = 0;
                y2 = 0;

                left = Math.Min(indexA.X, indexB.X) * size;
                bottom = Math.Min(indexA.Y, indexB.Y) * size + size;
            }
            else
            {
                //Vertical
                double highest = Math.Max(indexA.X, indexB.X);
                x1 = 0;
                x2 = 0;
                y1 = 0;
                y2 = size;

                left = Math.Min(indexA.X, indexB.X) * size + size;
                bottom = Math.Min(indexA.Y, indexB.Y) * size;
            }

            Line line = new Line { X1 = x1, Y1 = y1, X2 = x2, Y2 = y2};
            line.Stroke = Brushes.Black;
            line.StrokeThickness = 5;
            Canvas.SetLeft(line, left);
            Canvas.SetBottom(line, bottom);
            MicroCanvas.Children.Add(line);
        }

        private void _mouse_MovedEvent(object? sender, MovedEventArgs e)
        {
            MicroCanvas.Children.Remove(_mousePosition);

            double size = 1 * _gScale;

            double left = Math.Floor(e.Move.X) * size;
            double bottom = Math.Floor(e.Move.Y) * size;
            Canvas.SetLeft(_mousePosition, left);
            Canvas.SetBottom(_mousePosition, bottom);

            MicroCanvas.Children.Add(_mousePosition);
        }

        private void Maze_MazeFloodedEvent(object? sender, MazeFloodedEventArgs e)
        {
            foreach (TextBlock tb in _currentDistances)
                MicroCanvas.Children.Remove(tb);

            foreach (Cell cell in e.Cells)
            {
                TextBlock distance = new TextBlock();
                distance.Text = cell.DistanceToFinish.ToString();

                double size = 1 * _gScale;
                distance.Width = size;
                distance.Height = size;

                double left = cell.Index.X * _gScale + size / 2;
                double bottom = cell.Index.Y * _gScale - size / 2;
                Canvas.SetLeft(distance, left);
                Canvas.SetBottom(distance, bottom);

                MicroCanvas.Children.Add(distance);
                _currentDistances.Add(distance);
            }

        }

        private void Maze_MazeInitializedEvent(object? sender, MazeInitializedEventArgs e)
        {
            List<Cell> cells = e.Cells;
            foreach (Cell cell in cells)
            {
                DrawCell(cell);
            }
        }

        private void DrawCell(Cell cell)
        {
            Rectangle cellRect = new();

            double size = 1 * _gScale;
            cellRect.Width = size;
            cellRect.Height = size;
            cellRect.Stroke = cell.IsFinish ? Brushes.Red : Brushes.LightGray;

            double left = cell.Index.X * size;
            double bottom = cell.Index.Y * size;
            Canvas.SetLeft(cellRect, left);
            Canvas.SetBottom(cellRect, bottom);

            MicroCanvas.Children.Add(cellRect);
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            MicroCanvas.Background = Brushes.White;
            MicroCanvas.Children.Clear();

            _httpClient = new();
            _httpClient.BaseAddress = _configMap[HostConfiguration.Local];
            _httpClient.Timeout = TimeSpan.FromMinutes(5);

            UserIn userIn = new UserIn("plsPint", "pepe420420");
            _sessionService = new SessionService(_httpClient, userIn);
            string token = await _sessionService.Login();
            _mazeService = new(_httpClient, _sessionService);

            Console.WriteLine("Connected to MazeService");

            _mouse = new Mouse(_mazeService);
            await _mouse.Initialize(MAZE_SIZE);

            _mouse.Maze.MazeInitializedEvent += Maze_MazeInitializedEvent;
            _mouse.Maze.MazeFloodedEvent += Maze_MazeFloodedEvent;
            _mouse.Maze.Initialize();

            _mouse.MovedEvent += _mouse_MovedEvent;
            _mouse.UpdateUI += _mouse_UpdateUI; ;
            _mouse.Maze.WallAddedEvent += _mouse_WallAddedEvent;

            bool success = await _mouse.RunMaze();

            MicroCanvas.Background = success ? Brushes.ForestGreen :  Brushes.IndianRed;
        }

        private void _mouse_UpdateUI(object? sender, EventArgs e)
        {
            AllowUIToUpdate();
        }

        void AllowUIToUpdate()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Render, new DispatcherOperationCallback(delegate (object parameter)
            {
                frame.Continue = false;
                return null;
            }), null);

            Dispatcher.PushFrame(frame);
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background,
                                          new Action(delegate { }));
        }
    }
}
