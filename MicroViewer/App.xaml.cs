﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

using Services;
using MicroRunner;
using MicroRunner.Services.DTO;

namespace MicroViewer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly HttpClient _httpClient = new HttpClient();

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            MainWindow windows = new MainWindow();
        }
    }
}